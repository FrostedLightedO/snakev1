/*
 *    Sample snake problem using ncurses
 */

extern crate ncurses;

use ncurses::*;
use std::sync::mpsc;
use std::thread;
use std::time;
use term_basics_linux as tbl;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use std::process::Command;

const EXPLODE_DELAY_MILLIS : std::time::Duration = time::Duration::from_millis(200);
const NUMBER_OF_FOODS: i32 = 10;
const SCORE_TO_WIN : i32 = 10;
const WAIT_ENDING_MILLIS : u64 = WAIT_INTRO_MILLIS;
const WAIT_INTRO_MILLIS : u64 = 3000;
const WAIT_X_DELAY : u64 = 100;
const WAIT_Y_DELAY : u64 = 150;

#[derive(PartialEq, Eq, Clone, Copy)]
enum Direction { UP, DOWN, LEFT, RIGHT }

#[derive(PartialEq)]
enum Status { Success, OutOfBounds, HitSelf, Quit}

#[derive(PartialEq, Eq, Clone, Copy)]
struct Point {
  x:i32,
  y:i32
}

#[derive(PartialEq, Eq, Clone)]
struct Board {
  xmax:i32,
  ymax:i32,
  dir:Direction,
  snake:Vec<Point>,
  snake_has_grown:bool,
  foods:Vec<Point>,
  score:i32,
}

fn reset_terminal() {
    let output = if cfg!(target_os = "windows") {
        Command::new("cmd")
                .args(&["/C", "reset"])
                .output()
                .expect("failed to execute process")
    } else {
        Command::new("sh")
                .arg("-c")
                .arg("reset")
                .output()
                .expect("failed to execute process")
    };
    output.stdout;
}

fn explode(col:i32, row:i32) {
    mvaddstr(row - 2, col, " --- ");
    mvaddstr(row - 1, col, "-+++-");
    mvaddstr(row, col, "-+#+-");
    mvaddstr(row + 1, col, "-+++-");
    mvaddstr(row + 2, col, " --- ");
    refresh();
    thread::sleep(EXPLODE_DELAY_MILLIS);

    mvaddstr(row - 2, col, " +++ ");
    mvaddstr(row - 1, col, "++#++");
    mvaddstr(row, col, "+# #+");
    mvaddstr(row + 1, col, "++#++");
    mvaddstr(row + 2, col, " +++ ");
    refresh();
    thread::sleep(EXPLODE_DELAY_MILLIS);

    mvaddstr(row - 2, col, "  #  ");
    mvaddstr(row - 1, col, "## ##");
    mvaddstr(row, col, "#   #");
    mvaddstr(row + 1, col, "## ##");
    mvaddstr(row + 2, col, "  #  ");
    refresh();
    thread::sleep(EXPLODE_DELAY_MILLIS);

    mvaddstr(row - 2, col, " # # ");
    mvaddstr(row - 1, col, "#   #");
    mvaddstr(row, col, "     ");
    mvaddstr(row + 1, col, "#   #");
    mvaddstr(row + 2, col, " # # ");
    refresh();
    thread::sleep(EXPLODE_DELAY_MILLIS);
}

fn create_random_cell(board:&Board) -> Point{
    return Point {
      x: (rand::random::<u32>() % board.xmax as u32) as i32,
      y: (rand::random::<u32>() % board.ymax as u32) as i32
    };
}

// Add a new food that is not already a food or part of the snake
fn add_new_food(board:&mut Board) {
    let snake:Vec<Point> = board.snake.clone();
    loop {
        let new_food:Point = create_random_cell(board);
        if !board.foods.contains(&new_food) && !snake.contains(&new_food) {
            board.foods.push(new_food);
            break;
        }
    };
}

fn display_point(point:&Point, symbol:chtype) {
  mvaddch(point.y, point.x, symbol);
}

fn display_points(points:&Vec<Point>, symbol:chtype) {
  for v in points.iter() {
      display_point(&v, symbol);
  }
}

fn display_score(board:&Board) {
    let s = format!("Score:{}", board.score);
    mvaddstr(board.ymax*3/4, board.xmax*3/4, &s);
}

fn display_intro(board:&Board) {
    mvaddstr(board.ymax/2, board.xmax/2, "Welcome to snake");
    refresh();
    thread::sleep(time::Duration::from_millis(WAIT_INTRO_MILLIS));
    let s = format!("Can you score {} points ?", SCORE_TO_WIN);
    mvaddstr(board.ymax/2, board.xmax/2, &s);
    refresh();
    thread::sleep(time::Duration::from_millis(WAIT_INTRO_MILLIS));
}

fn get_next_move(k:char, board:&mut Board) {
    match k {
        'h' => {
            if board.dir == Direction::RIGHT  { return; }
            board.dir = Direction::LEFT;
        },
        'l' => {
            if board.dir == Direction::LEFT { return;}
            board.dir = Direction::RIGHT;
        },
        'k' => {
            if board.dir == Direction::DOWN { return;}
            board.dir = Direction::UP;
        },
        'j' => {
            if board.dir == Direction::UP { return;}
            board.dir = Direction::DOWN;
        },
        _ => { },
    }
}

fn move_snake(board:&mut Board) -> Status {
    assert!(board.snake.len() > 0);
    let snake_head = board.snake[0].clone();

    if !board.snake_has_grown {
        board.snake.pop();
    }

    match board.dir {
        Direction::RIGHT => {
            if snake_head.x == board.xmax { return Status::OutOfBounds; }
            board.snake.insert(0, Point {x:snake_head.x+1, y:snake_head.y});
        },
        Direction::LEFT => {
            if snake_head.x == 0 { return Status::OutOfBounds; }
            board.snake.insert(0, Point {x:snake_head.x-1, y:snake_head.y});
        },
        Direction::UP => {
            if snake_head.y == 0 { return Status::OutOfBounds; }
            board.snake.insert(0, Point {x:snake_head.x, y:snake_head.y-1});
        },
        Direction::DOWN => {
            if snake_head.y == board.ymax { return Status::OutOfBounds; }
            board.snake.insert(0, Point {x:snake_head.x, y:snake_head.y+1});
        }
    };

    // self snake collision detection
    for m in board.snake.iter().skip(1) {
        if *m == board.snake[0] {
            return Status::HitSelf;
        }
    }
    return Status::Success;
}

fn check_for_food(board:&mut Board) {
    assert!(board.snake.len() > 0);

    board.snake_has_grown = false;

    let snake_head = board.snake[0].clone();
    if board.foods.contains(&snake_head) {
        // Remove the eaten food
        let index = board.foods.iter().position(|x| *x == snake_head).unwrap();
        board.foods.remove(index);
        add_new_food(board);
        board.snake_has_grown = true;
        board.score += 1;
    }
}

fn create_snake(snake:&mut Vec<Point>) {
    snake.push(Point {x:2, y:4});
    snake.push(Point {x:2, y:3});
    snake.push(Point {x:2, y:2});
}

fn main() {
  let mut board = Board {
    xmax: 0,
    ymax: 0,
    dir: Direction::RIGHT,
    snake: Vec::new(),
    snake_has_grown: false,
    foods: Vec::new(),
    score: 0,
  };

  let should_stop = Arc::new(AtomicBool::new(false));
  let must_stop = Arc::clone(&should_stop);

  // Setup a signal catcher for interrupt
  ctrlc::set_handler(move || {
      must_stop.swap(true, Ordering::Relaxed);
  }).expect("Error setting Ctrl-C handler");

  let keypress_enabled = Arc::new(AtomicBool::new(true));
  let wait_for_keypress = Arc::clone(&keypress_enabled);

  let (tx, rx) = mpsc::channel();
  thread::spawn(move || {
      loop {
          let a = tbl::getch();
          tx.send(a).unwrap();
          if !wait_for_keypress.load(Ordering::Relaxed) {
              break;
          }
      }
  });

  /* Start ncurses. */
  initscr();
  keypad(stdscr(), true);
  noecho();
  curs_set(CURSOR_VISIBILITY::CURSOR_INVISIBLE);
  getmaxyx(stdscr(), &mut board.ymax, &mut board.xmax);
  display_intro(&board);

  create_snake(&mut board.snake);

  for _ in 1..NUMBER_OF_FOODS {
      add_new_food(&mut board);
  }

  // Game speed
  let mut x_delay_millis:time::Duration = time::Duration::from_millis(WAIT_X_DELAY);
  let mut y_delay_millis:time::Duration = time::Duration::from_millis(WAIT_Y_DELAY);
  let mut status;
  loop {
      clear();
      display_score(&board);
      display_points(&board.foods, ncurses::ACS_DIAMOND());
      display_points(&board.snake, ncurses::ACS_BLOCK());
      display_point(&board.snake[0], ncurses::ACS_CKBOARD());
      refresh();

      // X and Y movement a perceptively different speeds
      if board.score > 0 {
          x_delay_millis = time::Duration::from_millis(WAIT_X_DELAY/board.score as u64);
          y_delay_millis = time::Duration::from_millis(WAIT_Y_DELAY/board.score as u64);
      }
      match board.dir {
         Direction::LEFT | Direction::RIGHT => {
            thread::sleep(x_delay_millis);
         }
         Direction::UP | Direction::DOWN => {
            thread::sleep(y_delay_millis);
         }
      }

      // Check for keypress
      let keypress_rx = rx.try_recv();
      if keypress_rx.is_ok() {
          get_next_move(keypress_rx.unwrap() as char, &mut board);
      }
      check_for_food(&mut board);
      status = move_snake(&mut board);
      if should_stop.load(Ordering::Relaxed) {
          status = Status::Quit;
      }
      if board.score == SCORE_TO_WIN {
          display_score(&board);
          refresh();
          break;
      }
      if status != Status::Success { break; }
  }

  keypress_enabled.swap(false, Ordering::Relaxed);

  let last_msg = match status {
      Status::OutOfBounds => "Sorry..you went out of bounds !",
      Status::HitSelf => "Sorry..you cannot eat yourself !",
      Status::Success => "You WIN !",
      Status::Quit => "You QUIT ! Try again another time"
  };

  match status {
      Status::OutOfBounds | Status::HitSelf | Status::Quit => { explode(board.snake[0].x, board.snake[0].y) },
      _ => {}
  };
  mvaddstr(board.ymax/2, board.xmax/2, last_msg);
  let score_msg = format!("Final score:{}", board.score);
  mvaddstr(board.ymax/2+3, board.xmax/2, &score_msg);
  refresh();
  thread::sleep(time::Duration::from_millis(WAIT_ENDING_MILLIS));
  refresh();
  endwin();
  reset_terminal();
}
